import boto3
import botocore

s3 = boto3.client('s3')

response = s3.list_buckets()
buckets = [bucket['Name'] for bucket in response['Buckets']]

taglist = {}

for bucket in buckets:
    try:
        tags = s3.get_bucket_tagging(Bucket=bucket)

        for tag in tags['TagSet']:
            if tag['Key'] == 'Owner':
                taglist[bucket] = tag['Value']

    except botocore.exceptions.ClientError as e:
        taglist[bucket] = "N/A"

for entry in taglist:
    print("Bucket: {0:<32s}\tOwner: {1:>16s}".format(entry, taglist[entry]))

